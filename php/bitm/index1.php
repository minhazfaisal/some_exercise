<?php
class User {
    public $firstName;
    public $lastName;

    public function hello()
    {
        return "hello";
    }
}



$user1 = new User();

echo "<hr>";

$a = 4;
$b = "1a";
$add = $a+$b;
echo $add;
echo "<hr>";

$a = "b";
$b = "a";
$add = $a.$b;
echo $add;
echo "<hr>";

$a = 4;
$b = "1a";
$add = $a.$b;
echo $add;
echo "<hr>";

$color = "maroon";
$var = $color[2];
echo "$var";
echo "<hr>";
?>
