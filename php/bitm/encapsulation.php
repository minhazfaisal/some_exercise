<?php
/*encapsulation: Encapsulation or information hiding

Encapsulation is just wrapping some data in an object.
 The term “encapsulation” is often used interchangeably with “information hiding”.
Hiding the internals of the object protects its integrity by preventing users from
setting the internal data of the component into an invalid or inconsistent state.
You can use encapsulation if the properties of an object are private,
and the only way to update them is through public methods.

For example in this code we can protect our User objects from setting an invalid gender parameter.*/
class User {
    private $name;
    private $gender;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getGender() {
        return $this->gender;
    }

    public function setGender($gender) {
        if ('male' !== $gender and 'female' !== $gender) {
            throw new Exception('Set male or female for gender');
        }
        $this->gender = $gender;
        return $this;
    }
}

$user = new User();
$user->setName('Michal');
$user->setGender('male');