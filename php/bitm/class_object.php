<?php
// Declare the class
class Car {

    // properties
    public $comp;
    public $color = 'beige';
    public $hasSunRoof = true;

    // method that says hello
    public function hello()
    {
        return "beep";
    }
}

// Create an instance
$bmw = new Car ();
$mercedes = new Car ();

// Get the values
echo $bmw -> color; // beige
echo "<br />";
echo $mercedes -> color; // beige
echo "<hr />";

// Set the values
$bmw -> color = 'blue';
$bmw -> comp = "BMW";
$mercedes -> comp = "Mercedes Benz";

// Get the values again
echo $bmw -> color; // blue
echo "<br />";
echo $mercedes -> color; // beige
echo "<br />";
echo $bmw -> comp; // BMW
echo "<br />";
echo $mercedes -> comp; // Mercedes Benz
echo "<hr />";

// Use the methods to get a beep
echo $bmw -> hello(); // beep
echo "<br />";
echo $mercedes -> hello(); // beep


 echo "<hr>";///////////////////////////////////////

 class Sweet
 {
     public $taste = "sweet";
 }

 $rosmalai = new Sweet();
 $rosmalai->taste = "milky";
 $rosmalai->prize ="medium";
 echo $rosmalai ->taste;
echo $rosmalai ->prize;

echo "<hr>";//////////////////////////////////////////

class User {
    public $firstName;
    public $lastName;

    public function hello()
    {
        return "hello";
    }
}



$user1 = new User();
$user1->firstName = 'John';
$user1->lastName  = 'Doe';
//$user1->hello();
$hello = $user1->hello();

//echo $user1->hello() . ",  " . $user1->firstName . " " . $user1->lastName;
echo $hello .", ".$user1->firstName." ". $user1->lastName.".";