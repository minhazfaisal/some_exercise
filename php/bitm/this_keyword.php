<?php
class Car {

    // The properties
    public $comp;
    public $color = 'beige';
    public $hasSunRoof = true;

    // The method that says hello
    public function hello()
    {
        return "Beep I am a <i>" . $this -> comp . "</i>, and My color is <i>" . $this -> color."</i>";
    }
}

// We can now create an object from the class.
$bmw = new Car();
$mercedes = new Car();

// Set the values of the class properties.
$bmw -> color = 'blue';
$bmw -> comp = "BMW";
$mercedes -> comp = "Mercedes Benz";

// Call the hello method for the $bmw object.
echo $bmw -> hello();
echo "<hr>";

class User {

    // The class properties.
    public $firstName;

    public $lastName;

    // A method that says hello to the user $firstName.
    // The user $firstName property can be approached with the $this keyword.
    public function hello()
    {
        return "hello, " .  $this -> firstName;

    }
}

// Create a new object.
$user1 = new User();

// Set the user $firstName and $lastName properties.
$user1 -> firstName = "Jonnie";
$user1 -> lastName = "Roe";

// Echo the hello() method.
echo $user1 -> hello();
echo "<hr>";
//Chaining methods and properties
class Student {
    // The class properties.
    public $firstName;
    public $lastName;

    // A method that says hello to the user $firstName.
    // The user $firstName property can be approached with the $this keyword.
    public function hello()
    {
        return "hello, " .  $this -> firstName;

    }

    // The register method.
    public function register()
    {
        echo $this -> firstName . " " . $this -> lastName . " registered.";
        return $this;
    }

    // The mail method.
    public function mail()
    {
        echo " emailed.";
    }
}

$user1 = new Student();
$user1 -> firstName = "Jane";
$user1 -> lastName = "Roe";

// Chain the methods mail() to register().
$user1 -> register() -> mail();
echo "<hr>";

class MyCar {

    // public methods and properties.
    public $model;

    public function getModel()
    {
        return "The car model is " . $this -> model;
    }
}

$mercedes = new MyCar();
//Here we access a property from outside the class
$mercedes -> model = "Mercedes";
//Here we access a method from outside the class
echo $mercedes -> getModel();