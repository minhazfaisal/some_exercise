<?php

class User {
  private $username;
 
  public function setUsername($name) 
  {
    $this -> username = $name;
  }  
}
 
 
class Admin extends User {
  public function expressYourRoll()
  {
    return strtolower(__CLASS__);
  }
       
  public function sayHello()
  {
//    return "Hello admin, " . $this -> username;
  }
}
  
  
$admin1 = new Admin();
$admin1 -> setUsername("Balthazar");
$admin1 -> sayHello();

//The cause of the problem is that we try to access a private method, $username, from outside the class.

//////////////////////////////////////

//We can declare the $usrname property as protected to allow the 
//   access for properties and methods that belongs to the child classes
class Users {
  //Declare the $username as protected
  protected $username;
 
  public function setUsername($name) 
  {
    $this -> username = $name;
  }  
}
 
 
class Admins extends Users {
  public function expressYourRoll()
  {
    return strtolower(__CLASS__);
  }
      
  public function sayHello()
  {
    return "Hello admin, " . $this -> username;
  }
}
  
  
$admin1 = new Admins();
$admin1 -> setUsername("Balthazar");
echo $admin1 -> sayHello();