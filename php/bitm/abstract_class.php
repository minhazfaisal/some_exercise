<?php
abstract class Animal
{
public $name;
public $age;

public function Describe()
{
return $this->name . ", " . $this->age . " years old";
}

abstract public function Greet();
}

class Dog extends Animal
{
    public function Greet()
    {
        return "Woof!";
    }

    public function Describe()
    {
        return parent::Describe() . ", and I'm a dog!";
    }
}

$animal = new Dog();
$animal->name = "Bob";
$animal->age = 7;
echo $animal->Describe();
echo $animal->Greet();


echo "<hr>";///////////////////////////////////////////////////////////////////////

//Your practice code
 abstract class User{
     abstract public function stateYourRoll();

     protected $username;

     public function setUsername($name)
     {
         $this -> username =$name;
     }

     public function getUsername()
     {
         return $this -> username;
     }
 }

class Admin extends User {
    public function stateYourRoll()
    {
        return "admin";
    }
}

class Viewer extends User {
    public function stateYourRoll()
    {
        return strtolower(__CLASS__);
    }
}

$admin1 = new Admin();
$admin1 -> setUsername("Balthazar");
echo $admin1 -> stateYourRoll();
echo "<hr>";//////////////////////////////////////////////////////////////////////