PHP Superglobals The PHP Superglobals are a handful of arrays that provide to a PHP script global access
to data originating externally.

PHP Superglobals represent data coming from URLs, HTML forms, cookies, sessions, and the Web
server itself. $HTTP_GET_VARS, $HTTP_POST_VARS, etc., served these same purposes but
the PHP superglobal variables are better in that they can also be accessed within any functions.

Introduced in PHP 4.1, the Superglobals are:

	$_GET: The $_GET Superglobal represents data sent to the PHP script
	    in a URL. This applies both to directly accessed URLs
	    (e.g., http://www.example.com/page.php?id=2) and form submissions that use 	the GET method.

	$_POST: The $_POST Superglobal represents data sent to the PHP 	script via HTTP POST.
	    This is normally a form with a method of POST.

	$_COOKIE: The $_COOKIE Superglobal represents data available to a PHP script via HTTP cookies.

	$_REQUEST: The $_REQUEST Superglobal is a combination of $_GET, $_POST, and $_COOKIE.

	$_SESSION: The $_SESSION Superglobal represents data available to a PHP script that has
	    previously been stored in a session.

	$_SERVER: The $_SERVER Superglobal represents data available to a PHP script from the Web server itself.
	    Common uses of $_SERVER is to refer to the current PHP script ($_SERVER['PHP_SELF']),
	    the path on the server to that script, the host name, and so on.

	$_ENV: The $_ENV Superglobal represents data available to a PHP script from the environment
	    in which PHP is running.

	$_FILES: The $_FILES Superglobal represents data available to a PHP script from HTTP POST file uploads.
	    Using $_FILES is the currently preferred way to handle uploaded files in PHP.