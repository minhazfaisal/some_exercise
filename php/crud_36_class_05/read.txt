use student
CREATE TABLE student_info
( id int PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	email VARCHAR(100) NOT NULL,
	gender boolean NOT NULL, 
	age float NOT NULL, 
	pass varchar (100) NOT NULL
)	