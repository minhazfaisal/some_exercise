<?php
include "lib/connection.php";
$result="";
if (isset($_POST['submit_btn'])) {
	$name = $_POST['s_name'];
	$email = $_POST['s_email'];
	$pass = md5($_POST['s_pass']);
	$c_pass = md5($_POST['s_c_pass']);
	$gender = $_POST['s_gender'];
	$age = $_POST['s_age'];
	if ($pass == $c_pass) {
		
		$insertQ = "INSERT INTO student_info (name, email, gender,age,pass)
		VALUES ('$name','$email',$gender,$age,'$pass')";
		if ($conn->query($insertQ)) {
$result = "Student information added.";
		}else{
die($conn->error);
		}
	}else{
		$result= "password did not match. please try again";
	}
}
// select
$selectQ = "SELECT * FROM student_info";
$show_data = $conn->query($selectQ);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Crud Application</title>
	</head>
	<body>
		<form action=" <?php echo $_SERVER['PHP_SELF']; ?>" method="post">
			<label for="">Name</label> <br>
			<input type="text" name="s_name" placeholder="Your Name" required> <br>
			<label for="">Email</label> <br>
			<input type="email" name="s_email" placeholder="Your Email" required> <br>
			<label for="">Password</label> <br>
			<input type="password" name="s_pass" placeholder="Your Password" required> <br>
			<label for="">Confirm Password</label> <br>
			<input type="password" name="s_c_pass" placeholder="Confirm Email" required>
			<br>
			<label for="">Gender</label> <br>
			<select name="s_gender"> <br>
				<option value="0" selected>Male</option>
				<option value="1">Female</option>
			</select> <br>
			<label for="">Age</label> <br>
			<input type="text" name="s_age" placeholder="Your Age" required> <br> <br>
			<input type="submit" name="submit_btn" Value="Add Student"><br>
		</form>
		<br>
		<div class="result">
			<?php echo $result; ?>
		</div>
		<div class="show_data">
			<table border="1" cellpadding="10">
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Gender</th>
					<th>Age</th>
					<th>Action</th>
				</tr>
				<?php if ($show_data->num_rows>0) {?>
				<?php while ($row=$show_data->fetch_assoc()) {
					
				?>
				<tr>
					<td> <?php echo $row['name']; ?> </td>
					<td> <?php echo $row['email']; ?> </td>
					<td> <?php if ($row['gender']==0)
							{
						echo "male";
							}else {
						echo "female";
						} ?>
					</td>
					<td> <?php echo $row['age']; ?> </td>
					<td>
						<a href="lib/edit.php?id=<?php echo $row['id']; ?> ">Edit</a>
						<a href="lib/delete.php?id=<?php echo $row['id']; ?> ">Delete</a>
					</td>
				</tr>
				<?php } ?>
				<?php }else{ ?>
				<tr>
					<td colspan="5">No Data found</td>
				</tr>
				<?php } ?>
			</table>
		</div>
	</body>
</html>