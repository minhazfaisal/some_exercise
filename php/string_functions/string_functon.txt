The PHP strlen() function returns the length of a string.

The PHP str_word_count() function counts the number of words in a string:

The PHP strrev() function reverses a string:

The PHP strpos() function searches for a specific text within a string.
If a match is found, the function returns the character position of the first match.
If no match is found, it will return FALSE.

The PHP str_replace() function replaces some characters with some other characters in a string.

