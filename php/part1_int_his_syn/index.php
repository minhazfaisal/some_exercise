<?php
/**
 * Created by PhpStorm.
 * User: Hello
 * Date: 16-Jun-17
 * for: php syntax , introduction, history
 */

# This is a comment.

// Some comment.

/* This is
a multi-line
comment. */

//A PHP script starts with <?php and ends with ?>
<?php
// PHP code goes here
?>

<?php
echo "hello";
echo "<hr>";

$var = 1500;
echo "$var";
echo "<hr>";

$number = 12500;
echo "My number is $number";
echo "<hr>";

$numA = 120;
echo 'His Number is $numA';
echo "<hr>";

$numB = 1205;
echo 'Their Number is ' . $numB;
echo "<hr>";

$numC = 1205;
echo "Her Number is " . $numC;
echo "<hr>";

var_dump($number);
echo "<hr>";

$d = 12;
$e = 12;
$f = "a";
$g = "13";

echo $d+$e; //24
echo "<hr>";
echo $d+$e+$f;//24
echo "<hr>";
echo $d+$e+$g;//36
echo "<hr>";
echo $d+$g;//24
echo "<hr>";
echo $d+$f;//12
echo "<hr>";
echo "Number is " . $d.$e;//12
echo "<hr>";

$add = $d+ $e;//24
echo $add;
echo "<hr>";
$add = $d . $g;//1212
echo $add;
echo "<hr>";
$add = $d + $g;//25
echo $add;
echo "<hr>";
$adds = $g + $f;//13
echo $adds;
echo "<hr>";
















?>
