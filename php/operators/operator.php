<?php
$x = 20;
$x += 100;

echo $x;//120
echo "<hr>";//////////////
$a = 10;
$b = 20;
$a+=$b; //$a = $a + $b
echo $a;//30
echo "<hr>";//////////////
$x = 100;
$y = "100";
var_dump($x == $y); // returns true because values are equal//ans bool(true)
echo "<hr>";//////////////

$x = 100;
$y = "100";
var_dump($x === $y); // returns false because types are not equal //bool(false)
echo "<hr>";//////////////

$x = 100;
$y = "100";
var_dump($x != $y); // returns false because values are equal// bool(false)
echo "<hr>";//////////////

$x = 100;
$y = "100";
var_dump($x <> $y); // returns false because values are equal //bool(false)
echo "<hr>";//////////////
$x = 100;
$y = "100";
var_dump($x !== $y); // returns true because types are not equal// bool(true)
echo "<hr>";//////////////
$x = 100;
$y = 50;

var_dump($x > $y); // returns true because $x is greater than $y//bool(true)
echo "<hr>";//////////////
$x = 10;
$y = 50;

var_dump($x < $y); // returns true because $x is less than $y//bool(true)
echo "<hr>";//////////////
$x = 50;
$y = 50;

var_dump($x >= $y); // returns true because $x is greater than or equal to $y//bool(true)
echo "<hr>";//////////////
$x = 50;
$y = 50;

var_dump($x <= $y); // returns true because $x is less than or equal to $y//bool(true)
echo "<hr>";//////////////
echo "<hr>";//////////////

$x = 10;
echo ++$x;//11
echo "<hr>";//////////////
$x = 10;
echo $x++;//10
echo "<hr>";//////////////
$x = 10;
echo --$x;//9
echo "<hr>";//////////////
$x = 10;
echo $x--;//10
echo "<hr>";//////////////
echo "<hr>";//////////////

$x = 100;
$y = 50;

if ($x == 100 and $y == 50) {
    echo "Hello world!";
}//Hello world!
echo "<hr>";//////////////
$x = 100;
$y = 50;

if ($x == 100 or $y == 80) {
    echo "Hello world!";
}//Hello world!
echo "<hr>";//////////////
$x = 100;
$y = 50;

if ($x == 100 xor $y == 80) {
    echo "Hello world!";
}//Hello world!
echo "<hr>";//////////////
$x = 100;
$y = 50;

if ($x == 100 && $y == 50) {
    echo "Hello world!";
}//Hello world!
echo "<hr>";//////////////
$x = 100;
$y = 50;

if ($x == 100 || $y == 80) {
    echo "Hello world!";
}//Hello world!
echo "<hr>";//////////////
$x = 100;

if ($x !== 90) {
    echo "Hello world!";
}//Hello world!
echo "<hr>";//////////////
echo "<hr>";//////////////
$txt1 = "Hello";
$txt2 = " world!";
$txt1 .= $txt2;
echo $txt1;//Hello world!
echo "<hr>";//////////////
$txt1 = "Hello";
$txt2 = " world!";
echo $txt1 . $txt2;//Hello world!
echo "<hr>";//////////////
echo "<hr>";//////////////
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

print_r($x + $y); // union of $x and $y//Array ( [a] => red [b] => green [c] => blue [d] => yellow )
echo "<hr>";//////////////
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

var_dump($x == $y);//bool(false)
echo "<hr>";//////////////
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

var_dump($x === $y);//bool(false)
echo "<hr>";//////////////
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

var_dump($x != $y);//bool(true)
echo "<hr>";//////////////
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

var_dump($x <> $y);//bool(true)
echo "<hr>";//////////////
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

var_dump($x !== $y);//bool(true)
echo "<hr>";//////////////

echo "<hr>";//////////////

echo "<hr>";//////////////

echo "<hr>";//////////////

echo "<hr>";//////////////

echo "<hr>";//////////////



