<?php
/**
 * Created by PhpStorm.
 * User: Hello
 * Date: 16-Jun-17
 * for: php variable , data type, constant
 */
?>

<?php

$a = "Hello world!";
var_dump($a);
echo "<hr>";

$b = 2;
var_dump($b);
echo "<hr>";

$x = null;
var_dump($x);
echo "<hr>";

$q = 0;
$w = 0;
var_dump(1213 <= 12125);//bool(true)
echo "<hr>";
var_dump(1212 == 1212);//bool(true)
echo "<hr>";
var_dump("1212" == 1212);//bool(true)
echo "<hr>";
var_dump("ab" >= "ab");//bool(true)
echo "<hr>";
var_dump("1212" == "ab");//bool(false)
echo "<hr>";

//object
class Car {
    function Car() {
        $this->model = "VW";
    }
}

// create an object
$herbie = new Car();

// show object properties
echo $herbie->model;
?>

<?php
$cars = array("Volvo","BMW","Toyota");
var_dump($cars);
?>

<?php
//float
$h = 10.365;
var_dump($h);
echo "<hr>";
///////////////////////////////////////////////////////////////////////
$j = 50; // global scope
function myTest() {
    // using x inside this function will generate an error
    echo "<p>Variable j inside function is: $j</p>";//notice Undefined variable: j
}
myTest();

echo "<p>Variable j outside function is: $j</p>";
echo  "<hr>";
/////////////////////////////////////////////////////////////////////
function myTests() {
    $k = 5; // local scope
    echo "<p>Variable k inside function is: $k</p>";
}
myTests();

// using x outside the function will generate an error
echo "<p>Variable k outside function is: $k</p>";//Notice: Undefined variable: k
echo  "<hr>";

//<!--The global Keyword-->///////////////////////////

$o = 5;
$p = 10;

function myTestA() {
    global $o, $p;
    $p = $o + $p;
}

myTestA();
echo $p; // outputs 15
echo "<hr>";
//////////////////////// $GLOBALS[index]
$y = 15;
$u = 10;

function myTestf() {
    $GLOBALS['y'] = $GLOBALS['y'] + $GLOBALS['u'];
}

myTestf();
echo $y; // outputs 25
echo "<hr>";


//<!--The static Keyword-->///////////////////////
//Normally, when a function is completed/executed, all of its variables are deleted.
//However, sometimes we want a local variable NOT to be deleted. We need it for a further job.
//To do this, use the static keyword when you first declare the variable:

function myTestB() {

    static $xe = 0;

    echo $xe;
    $xe++;
    echo "<hr>";
}

myTestB();//0

myTestB();//1

myTestB();//2
//Then, each time the function is called, that variable will still have the information it contained from
//the last time the function was called.
//Note: The variable is still local to the function.

?>
<?php
$hello = "Hello PHP";

echo "Output is " . $hello. " how are you";
echo "<hr>";
echo ("hello" .$hello);
print "hello".$hello;