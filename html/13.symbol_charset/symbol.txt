HTML Symbol Entities:

Many mathematical, technical, and currency symbols, are not present on a normal keyboard.
To add such symbols to an HTML page, you can use an HTML entity name.
If no entity name exists, you can use an entity number, a decimal, or hexadecimal reference.

